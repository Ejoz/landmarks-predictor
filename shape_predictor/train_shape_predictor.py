import multiprocessing
import argparse
import dlib

# Argument parser
ap = argparse.ArgumentParser()
ap.add_argument("-t", "--training", required=True,
	help="path to input training XML file")
ap.add_argument("-m", "--model", required=True,
	help="path serialized dlib shape predictor model")
args = vars(ap.parse_args())

# Default options for dlib's shape predictor
print("[INFO] setting shape predictor options...")
options = dlib.shape_predictor_training_options()

# Define the depth of each regression tree
options.tree_depth = 4

# Regularization parameter in the range [0, 1]
options.nu = 0.1

# The number of cascades used to train the shape predictor
options.cascade_depth = 15

# Number of pixels used to generate features for the random trees at
# each cascade
options.feature_pool_size = 400

# Selects best features at each cascade when training
options.num_test_splits = 50

# Controls amount of "jitter" (i.e., data augmentation)
options.oversampling_amount = 5

# Amount of translation jitter to apply
options.oversampling_translation_jitter = 0.1

# Tell the dlib shape predictor to be verbose
options.be_verbose = True

# Number of threads/CPU cores to be used when training
options.num_threads = multiprocessing.cpu_count()

# Log our training options to the terminal
print("[INFO] shape predictor options:")
print(options)

# Train the shape predictor
print("[INFO] training shape predictor...")
dlib.train_shape_predictor(args["training"], args["model"], options)