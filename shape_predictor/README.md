## Shape Predictor

Train a custom shape predictor to detect only choosen parts of a face.
/!\ Don't forget to change landmarks points according to your needs.

## Installation steps

0. Activate conda environment
```
conda activate dlib
```

1. Install Dlib
```
conda install -c anaconda cmake
conda install -c conda-forge dlib
```

2. Install Open CV
```
pip install opencv-contrib-python
pip install imutils
```

3. Download the iBUG 300-W dataset
```
wget http://dlib.net/files/data/ibug_300W_large_face_landmark_dataset.tar.gz
tar -xvf ibug_300W_large_face_landmark_dataset.tar.gz
```

## Parse XML files to keep only eyes points
```bash
# Train
python parse_xml.py \
	--input ibug_300W_large_face_landmark_dataset/labels_ibug_300W_train.xml \
	--output ibug_300W_large_face_landmark_dataset/labels_ibug_300W_train_mouth.xml

# Test
python parse_xml.py \
	--input ibug_300W_large_face_landmark_dataset/labels_ibug_300W_test.xml \
	--output ibug_300W_large_face_landmark_dataset/labels_ibug_300W_test_mouth.xml
```

## Train custom shape predictor
```bash
python train_shape_predictor.py \
	--training ibug_300W_large_face_landmark_dataset/labels_ibug_300W_train_mouth.xml \
	--model mouth_predictor.dat
```

## Evaluate the model
```bash
# Train
python evaluate_shape_predictor.py --predictor mouth_predictor.dat \
	--xml ibug_300W_large_face_landmark_dataset/labels_ibug_300W_train_mouth.xml

# Test
python evaluate_shape_predictor.py --predictor mouth_predictor.dat \
	--xml ibug_300W_large_face_landmark_dataset/labels_ibug_300W_test_mouth.xml
```

### Hyperparameters
- Training with cascade depth: 15
- Training with tree depth: 4
- Training with 500 trees per cascade level.
- Training with nu: 0.1
- Training with random seed: 
- Training with oversampling amount: 5
- Training with oversampling translation jitter: 0.1
- Training with landmark_relative_padding_mode: 1
- Training with feature pool size: 400
- Training with feature pool region padding: 0
- Training with 8 threads.
- Training with lambda_param: 0.1
- Training with 50 split tests.

### Results
MAE: Mean Average Error

| Face part    | Training MAE | Testing MAE | Date       |
|:-------------|:------------:|:-----------:|-----------:|
| Eyes         |  ~3.65       |  ~7.41      | 03/08/2020 |
| Eyebrows     |  ~7.19       |  ~13.93     | 04/20/2020 |
| Mouth        |  ~5.43       |  ~10.83     | 04/20/2020 |

## Visualize results in streaming video
```
python predict_eyes.py --shape-predictor mouth_predictor.dat
```

# Dlib landmarks
![Landmarks](facial_landmarks_68markup.jpg)
