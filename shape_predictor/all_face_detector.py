import numpy as np
import dlib
import cv2


def rect_to_bb(rect):
	""" Takes dlib prediction and convert it to OpenCV format.

	Args:
		rect (bounding box rectangle): dlib prediction of the face

	Returns:
		coords (tuple): 4-tuple of coordinates (x, y, w, h)
	"""
	x = rect.left()
	y = rect.top()
	w = rect.right() - x
	h = rect.bottom() - y
	return (x, y, w, h)


def shape_to_np(shape, dtype="int"):
	""" Converts shape object into NumPy array.

	Args:
		shape (object): Contains 68 coordinates (x, y) of facial landmarks region.
		dtype (str, optional): Defaults to "int".

	Returns:
		coords (array): NumPy array of coordinates
	"""
	coords = np.zeros((68, 2), dtype=dtype)

	for i in range(0, 68):
		coords[i] = (shape.part(i).x, shape.part(i).y)

	return coords


# Initialize dlib's face detector (HOG-based)
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("app/models/shape_predictor_68_face_landmarks.dat")

cap = cv2.VideoCapture("images/ohmucestlavie.mp4")

while (cap.isOpened()):
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    rects = detector(gray, 1)

    for rect in rects:
        shape = predictor(gray, rect)
        shape = shape_to_np(shape)

        for (x, y) in shape:
            cv2.circle(frame, (x, y), 1, (0, 0, 255), -1)
	
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF

	# Press 'q' to exit
    if key == ord("q"):
        break

cap.release()
cv2.destroyAllWindows()
