import streamlit as st
from core import LandmarksModels
from utils import get_file_content_as_string, load_image, display_image_with_landmarks

EYES_PREDICTOR = "app/models/eye_predictor.dat"
EYEBROWS_PREDICTOR = "app/models/eyebrow_predictor.dat"
MOUTH_PREDICTOR = "app/models/mouth_predictor.dat"
ALL_FACE_PREDICTOR = "app/models/shape_predictor_68_face_landmarks.dat"


def main():
    """ Landmarks Predictor App """

    st.set_option('deprecation.showfileUploaderEncoding', False)

    st.markdown(get_file_content_as_string("app/presentation.md"))
    predictor_choice = st.selectbox("Please select the facial zone you want to put landmarks on.", ("Eyes", "Eyebrows", "Mouth", "All face"))
    image_file = st.file_uploader("Upload Image", type=['jpg','png','jpeg'])

    if image_file is not None:
        lm = LandmarksModels()
        img = load_image(image_file)

        if predictor_choice == "Eyes":
            predictor = EYES_PREDICTOR
        elif predictor_choice == "Eyebrows":
            predictor = EYEBROWS_PREDICTOR
        elif predictor_choice == "Mouth":
            predictor = MOUTH_PREDICTOR
        elif predictor_choice == "All face":
            predictor = ALL_FACE_PREDICTOR

        output_img = lm.predict_landmarks(img, predictor)
        display_image_with_landmarks(output_img)
        

if __name__ == "__main__":
    main()