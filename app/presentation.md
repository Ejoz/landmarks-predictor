# Facial Landmarks Predictor

This app presents a demo of facial landmarks automatic detection.

It uses models trained on 3000 images from the [iBug 300-W dataset](https://ibug.doc.ic.ac.uk/resources/300-W/).

I splitted them in three models (eyes, eyebrows and mouth) *partly* because I wanted to know how to train specific models, *mainly* for fun.

There is also the pre-trained model of dlib for a full-face landmarks detection.

You can find the source code on [GitLab](https://gitlab.com/Ejoz/landmarks-predictor).

### Models evaluation
*MAE: Mean Average Error*

| Face part    | Training MAE | Testing MAE | Training date |
|:-------------|:------------:|:-----------:|--------------:|
| Eyes         |  ~3.65       |  ~7.41      | 03/08/2020    |
| Eyebrows     |  ~7.19       |  ~13.93     | 04/08/2020    |
| Mouth        |  ~5.43       |  ~10.83     | 04/08/2020    |


Enjoy this app, and if you have any questions, please feel free to contact me via [LinkedIn](https://www.linkedin.com/in/chlolaurent/).

## Landmarks Predictor

