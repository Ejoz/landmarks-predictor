import dlib
import cv2
import numpy as np

EYES_PREDICTOR = "app/models/eye_predictor.dat"
EYEBROWS_PREDICTOR = "app/models/eyebrow_predictor.dat"
MOUTH_PREDICTOR = "app/models/mouth_predictor.dat"
ALL_FACE_PREDICTOR = "app/models/shape_predictor_68_face_landmarks.dat"


class LandmarksModels:

    def __init__(self):
        pass

    def load_face_detector(self):
        return dlib.get_frontal_face_detector()

    def load_predictor(self, model_path):
        return dlib.shape_predictor(model_path)

    def shape_to_np(self, shape, dtype="int"):
        """ Converts shape object into NumPy array.

        Args:
            shape (object): Contains n coordinates (x, y) of facial landmarks region.
            dtype (str, optional): Defaults to "int".

        Returns:
            coords (array): NumPy array of coordinates
        """
        coords = np.zeros((shape.num_parts, 2), dtype=dtype)

        for i in range(0, shape.num_parts):
            coords[i] = (shape.part(i).x, shape.part(i).y)

        return coords

    def predict_landmarks(self, array_img, model_path):
        """ Predict landmarks on the face

        Args:
            array_img (array): Array image
            model_path (str): Path to the model

        Returns:
            array_img (array): array image with landmarks
        """
        detector = self.load_face_detector()
        predictor = self.load_predictor(model_path)

        gray = cv2.cvtColor(array_img, cv2.COLOR_BGR2GRAY)
        rects = detector(gray, 0)

        for rect in rects:
            shape = predictor(gray, rect)
            shape = self.shape_to_np(shape)

            for (sX, sY) in shape:
                cv2.circle(array_img, (sX, sY), 3, (158, 253, 56), -1)

        return array_img


def main():
    # Initialize the class
    lm = LandmarksModels()

    # Test image
    img_test = "images/ks.jpg"
    img = cv2.imread(img_test, 1)

    # Predict the landmarks
    img = lm.predict_landmarks(img, ALL_FACE_PREDICTOR)

    # Display result
    cv2.imshow("img", img)
    key = cv2.waitKey(0) 
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()



