import streamlit as st
import numpy as np
import cv2


def load_image(filepath):
    """ Load and transform an image

    Args:
        filepath (path): Uploaded file

    Returns:
        img: Array format of the image
    """
    file_bytes = np.asarray(bytearray(filepath.read()), dtype=np.uint8)
    img = cv2.imdecode(file_bytes, 1)
    return img


def display_image_with_landmarks(cv_img):
    """ Converts an image from BGR to RGB
        and display it in streamlit format

    Args:
        cv_img (array): Image to display
    """
    cv_img = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
    st.image(cv_img, use_column_width=True)


@st.cache(show_spinner=False)
def get_file_content_as_string(path):
    """ Get the content of a text file

    Args:
        path (markdown file): File path

    Returns:
        text: Markdown ready for display
    """
    text = open(path, "r")
    return text.read()
