FROM python:3.7

EXPOSE 8501

WORKDIR /app

ADD . /app/

RUN apt-get update -y \
&& apt-get install -y build-essential cmake \
&& apt-get install -y libopenblas-dev liblapack-dev \
&& apt-get install -y libx11-dev libgtk-3-dev \
&& pip install --upgrade pip \
&& pip install -r requirements.txt

CMD streamlit run app/app.py
