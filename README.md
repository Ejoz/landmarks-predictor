# Computer Vision in a streamlit little box

This app presents a way to visualize landmarks prediction on a face.
I used [streamlit](https://www.streamlit.io/) to integrate the code into a simple application.

## Run  it yourself !
1. [With docker-compose](#run-with-docker-compose)
1. [With docker](#run-with-docker)
1. [Locally](#run-locally)

### App tree
```
├── app
│   ├── app.py                          # Main file (Streamlit engine)
│   ├── core
│   │   ├── facial_landmarks            # Facial landmarks package
│   │   └── __init__.py
│   ├── models                          # .dat files
│   ├── presentation.md
│   └── utils.py
├── docker-compose.yml
├── Dockerfile
├── images
├── LICENSE
├── README.md
├── requirements.txt
└── shape_predictor                     # Training folder
```

## Run with docker-compose
You'll need [docker](https://docs.docker.com/engine/) and [docker-compose](https://docs.docker.com/compose/) installed on your machine.

1. Clone the repository
```bash
git clone https://gitlab.com/Ejoz/landmarks-predictor.git
```

2. Run
```bash
docker-compose up
```

## Run with docker
You'll need [docker](https://docs.docker.com/engine/) installed on your machine.

1. Clone the repository
```bash
git clone https://gitlab.com/Ejoz/landmarks-predictor.git
```

2. Build the image
```bash
docker build -f Dockerfile -t app:latest .
```

3. Run the container
```bash
docker run -p 8501:8501 app:latest
```

## Run locally

1. Clone the repository
```bash
git clone https://gitlab.com/Ejoz/landmarks-predictor.git
```

2. Create a virtual environment with all the dependencies.
   1. *With anaconda*
    ```bash
    conda create $YOUR_VIRTUAL_ENV
    conda activate $YOUR_VIRTUAL_ENV
    conda forge -c cmake
    conda install -c conda-forge dlib
    pip install -r requirements.txt
    ```

   2. *With virtualenv (you'll need preliminary steps for dlib installation)*
    ```bash
    sudo apt-get update
    sudo apt-get install build-essential cmake
    sudo apt-get install libopenblas-dev liblapack-dev 
    sudo apt-get install libx11-dev libgtk-3-dev
    sudo apt-get install python3 python3-dev python3-pip
    # You may need to restart your terminal after these steps.

    python3 -m venv $YOUR_VIRTUAL_ENV
    source $YOUR_VIRTUAL_ENV/bin/activate
    pip install -r requirements.txt
    ```

3. You can now run the app
```bash
streamlit run app/app.py
```

## Models evaluation
*MAE: Mean Average Error*

If you want to train your own models, please feel free to go to the [training folder](/shape_predictor/README.md) with its dedicated instructions.

| Face part    | Training MAE | Testing MAE | Date       |
|:-------------|:------------:|:-----------:|-----------:|
| Eyes         |  ~3.65       |  ~7.41      | 03/08/2020 |
| Eyebrows     |  ~7.19       |  ~13.93     | 04/20/2020 |
| Mouth        |  ~5.43       |  ~10.83     | 04/20/2020 |



### Enjoy the facial landmarks :)
![Kristen gif](images/landmarks_gif.gif)


# __Bonus__: How to deploy a dockerized app to [Heroku (AWS)](https://devcenter.heroku.com/)

It took me a while to figure it out, so I'm sharing these tips, hoping this will help some others to achieve a complete demo :)

*You'll need to be on your ***main*** branch to do this, otherwise it won't work.*

First, make sure your Dockerfile is running locally. You can now create a ```heroku.yml``` file.

```yml
build:
  docker:
    web: Dockerfile
```

```bash
heroku container:login
heroku container:push web
heroku container:release web
```

[See Heroku documentation.](https://devcenter.heroku.com/articles/build-docker-images-heroku-yml)
